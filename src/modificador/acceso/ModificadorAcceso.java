package modificador.acceso;

public class ModificadorAcceso {

    public static final String NOMBRE_APLICACION = "onboarding";
    private static final String SALUDO = "Hola ";
    protected static final int EDAD = 32;


    public String saludo(String nombre) {
        return SALUDO + nombre;
    }

    public static String saludar(String nombre) {
        return SALUDO + nombre;
    }

    private boolean esMayorQue10(int numero) {
        return numero > 10;
    }

    protected int sumar(int numero1, int numero2) {
        return numero1 + numero2;

    }


}
