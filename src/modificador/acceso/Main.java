package modificador.acceso;

public class Main {

    public static void main(String[] args) {
        String saludo1 = ModificadorAcceso.saludar("Pablo");

        ModificadorAcceso modificador = new ModificadorAcceso();

        System.out.println(modificador.sumar(10, 20));

        System.out.println(saludo1);
        System.out.println(ModificadorAcceso.NOMBRE_APLICACION);
    }

}
