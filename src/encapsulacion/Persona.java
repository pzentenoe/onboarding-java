package encapsulacion;

public class Persona {

    private String nombre;
    private int edad;
    private boolean esMayorDeEdad;


    public String getNombre() {
        return this.nombre;
    }

    void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
        this.esMayorDeEdad = edad >= 18;
    }

    public boolean isEsMayorDeEdad() {
        return esMayorDeEdad;
    }


    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", edad=" + edad +
                ", esMayorDeEdad=" + esMayorDeEdad +
                '}';
    }
}
