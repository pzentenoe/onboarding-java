package encapsulacion;

public class Main {

    public static void main(String[] args) {
        Persona p = new Persona();

        p.setNombre("Claudio");
        p.setEdad(30);


        System.out.println(p.getNombre());
        System.out.println(p.getEdad());
        System.out.println(p.isEsMayorDeEdad());
    }
}
