package wrapperclass;

import encapsulacion.Persona;

public class Main {

    public static void main(String[] args) {

        Float varFloat = 100.3f;

        Integer varInteger = 1000;

        String nuevo = varInteger.toString();

        String numeroString = "10asasas0";


        try {
            int numero = Integer.parseInt(numeroString);
            System.out.println(numero);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


        try {
            Persona p = null;
            p.getEdad();
        } catch (NullPointerException e) {
            System.out.println("NullPointer");
            System.out.println(e.getLocalizedMessage());
        }


    }
}
