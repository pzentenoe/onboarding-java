package tiposdatos;

//Tipos de datos primitivos
public class TiposDatos {

    public static void main(String[] args) {


        //Sin decimales
        byte numeroByte = -128;     //8 bits	-128	127
        short numeroShort = 32767;  //16 bits	-32768	32767
        int numeroInt = 299;        //32 bits	-2147483648	2147483647
        long numeroLong = 1000;     //64 bits	-9223372036854775808	9223372036854775807

        //Numeros con decimales
        float numeroFloat = 100.2f;    //32 bits	-3.402823e38	3.402823e38
        double numeroDouble = 1000.2;  //64 bits	-1.79769313486232e308	1.79769313486232e308

        //1 Caracter
        char letra = 'a';   //'\u0000'	'\uffff'

        //Boolean
        boolean isTrue = false;  // true false

        System.out.println(isTrue);


    }


}
