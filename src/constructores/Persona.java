package constructores;

public class Persona {

    private String nombre;
    private int edad;
    private boolean esMayorDeEdad;


    public Persona() {
    }

    public Persona(String nombre) {
        this.nombre = nombre;
    }

    public Persona(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    public Persona(String nombre, int edad, boolean esMayorDeEdad) {
        this.nombre = nombre;
        this.edad = edad;
        this.esMayorDeEdad = esMayorDeEdad;
    }


    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", edad=" + edad +
                ", esMayorDeEdad=" + esMayorDeEdad +
                '}';
    }
}
