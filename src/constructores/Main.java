package constructores;

public class Main {

    public static void main(String[] args) {

        Persona p1 = new Persona();

        System.out.println(p1);


        Persona p2 = new Persona("Pablo");
        System.out.println(p2);


        Persona p3 = new Persona("Alejandro", 14);
        System.out.println(p3);


        Persona p4 = new Persona("Ina", 10);
        System.out.println(p4);

    }
}
